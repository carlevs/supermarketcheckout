# Supermarket Checkout
Challenge to calculate total price for a number of items with relevant discounts in a C# console application.

## Assumptions
1. Discount is applied after all items are added by calculating an order item / line item.
2. Only one discount per product.

## Design approach
All products and discounts to be stored in database/file to allow for configuration (hardcoded list applied for example).

## Implementation
Dependency injection applied in Program.cs via IoC Container using autofac (https://autofaccn.readthedocs.io/en/latest/index.html) to register dependencies.
Logging added with NLog (https://nlog-project.org/).

## Unit Testing
Applied in separate project within solution using NUnit (https://nunit.org/) and Moq (https://github.com/Moq/moq4/wiki/Quickstart).
