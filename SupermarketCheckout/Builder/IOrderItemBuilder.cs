﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Builder
{
    public interface IOrderItemBuilder
    {
        IList<OrderItem> Build(IList<Product> Products, IList<Discount> discounts);
    }
}
