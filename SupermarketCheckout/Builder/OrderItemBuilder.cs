﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupermarketCheckout.Core.Calculator;
using SupermarketCheckout.Core.Counter;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Builder
{
    public class OrderItemBuilder : IOrderItemBuilder
    {
        readonly IListGroupCounter<Product> ProductCounter;
        readonly IPriceCalculatorFactory PriceCalculatorFactory;
        readonly ILogger Logger;

        public OrderItemBuilder(IListGroupCounter<Product> counter,
            IPriceCalculatorFactory priceCalculatorFactory,
            ILogger logger)
        {
            ProductCounter = counter;
            PriceCalculatorFactory = priceCalculatorFactory;
            Logger = logger;
        }

        /// <summary>
        /// Builds an order item from a list of products and current discounts.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="discounts"></param>
        /// <returns></returns>
        public IList<OrderItem> Build(IList<Product> products,
            IList<Discount> discounts)
        {
            if (products == null)
                throw new ArgumentNullException($"{products} cannot be null");

            Logger.Log(LogLevel.INFO, typeof(OrderItemBuilder), "Building order items");

            // Group products by count
            IDictionary<Product, int> productTotals = ProductCounter.Group(products);

            return BuildOrderItemList(productTotals, discounts);
        }

        /// <summary>
        /// Takes grouped products and a list of discounts and creates an order
        /// item with the relevant price applied.
        /// </summary>
        /// <param name="productTotals"></param>
        /// <param name="discounts"></param>
        /// <returns></returns>
        private IList<OrderItem> BuildOrderItemList(IDictionary<Product, int> productTotals,
            IList<Discount> discounts)
        {
            // Create a list of order items from the products and discounts
            var orderItemList = new List<OrderItem>();
            foreach (KeyValuePair<Product, int> entry in productTotals)
            {
                Product product = entry.Key;
                var orderItem = new OrderItem
                {
                    Product = product,
                    Quantity = entry.Value
                };

                // Calculate order item price based on whether the product
                // has an active discount
                Discount activeDiscount = discounts.ToList().Find(p => p.Sku == product.Sku);
                IPriceCalculator priceCalculator = PriceCalculatorFactory.GetPriceCalculator(activeDiscount,
                    Logger);
                orderItem.Price = priceCalculator.Calculate(orderItem.Quantity, product.Price);

                Logger.Log(LogLevel.INFO, typeof(OrderItemBuilder),
                    $"Adding order item {orderItem} to list");

                orderItemList.Add(orderItem);
            }
            return orderItemList;
        }
    }
}
