﻿using System;
namespace SupermarketCheckout
{
    public class CheckoutException : Exception
    {
        public CheckoutException()
        {
        }

        public CheckoutException(string message) : base(message)
        {
        }

        public CheckoutException(string message, Exception e) : base(message, e)
        {
        }
    }
}
