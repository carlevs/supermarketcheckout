﻿using System;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Calculator
{
    public class DiscountPriceCalculator : IPriceCalculator
    {
        private readonly Discount Discount;
        readonly ILogger Logger;

        public DiscountPriceCalculator(Discount discount, ILogger logger)
        {
            if (discount.Quantity < 1)
                throw new ArgumentException($"{discount.Quantity} must be greater than zero");

            if (discount.Price <= 0)
                throw new ArgumentException($"{discount.Price} must be greater than zero");

            Discount = discount;
            Logger = logger;
        }

        /// <summary>
        /// Calculates a discounted price if conditions are met for discount.
        /// </summary>
        /// <param name="count"></param>
        /// <param name="price"></param>
        /// <returns>Discount price</returns>
        public decimal Calculate(int count, decimal price)
        {
            if (price <= 0)
                throw new ArgumentException($"{price} must be greater than zero");

            if (count < 1)
                throw new ArgumentException($"{count} must be greater than zero");

            decimal discountPrice = 0m;

            // Calculate the no of discounts that need to be applied at the
            // discount price.
            int noOfDiscounts = count / Discount.Quantity;
            if (noOfDiscounts >= 1)
            {
                Logger.Log(LogLevel.INFO, typeof(DiscountPriceCalculator),
                    $"{noOfDiscounts} discounts found");

                discountPrice = noOfDiscounts * Discount.Price;

                // Add any remainder at full price
                int remainder = count % Discount.Quantity;
                if (remainder > 0)
                {
                    discountPrice += remainder * price;
                }
            }
            else
            {
                discountPrice = count * price;
            }

            return discountPrice;
        }
    }
}
