﻿namespace SupermarketCheckout.Core.Calculator
{
    public interface IPriceCalculator
    {
        decimal Calculate(int count, decimal price);
    }
}
