﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Core.Calculator
{
    public interface IPriceCalculatorFactory
    {
        IPriceCalculator GetPriceCalculator(Discount discount, ILogger logger);
    }
}
