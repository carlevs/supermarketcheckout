﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Core.Calculator
{
    public class PriceCalculatorFactory : IPriceCalculatorFactory
    {
        public IPriceCalculator GetPriceCalculator(Discount discount, ILogger logger)
        {
            if (discount == null)
            {
                return new StandardPriceCalculator();
            }

            return new DiscountPriceCalculator(discount, logger);
        }
    }
}
