﻿using System;

namespace SupermarketCheckout.Core.Calculator
{
    public class StandardPriceCalculator : IPriceCalculator
    {
        public decimal Calculate(int count, decimal price)
        {
            if (price <= 0)
                throw new ArgumentException($"{price} must be greater than zero");

            if (count < 1)
                throw new ArgumentException($"{count} must be greater than zero");

            return count * price;
        }
    }
}
