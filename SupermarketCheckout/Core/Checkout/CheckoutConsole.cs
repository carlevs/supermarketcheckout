﻿using System;

namespace SupermarketCheckout.Core.Checkout
{
    public class CheckoutConsole : IConsole
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public void Quit()
        {
            Environment.Exit(0);
        }
    }
}
