﻿using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Service;

namespace SupermarketCheckout.Core.Checkout
{
    public class CheckoutScanner : ICheckoutScanner
    {
        readonly ILogger Logger;
        readonly IOperationFactory OperatorFactory;
        readonly IConsole CheckoutConsole;
        readonly IProductService ProductService;
        readonly IOrderService OrderService;
        readonly IDiscountService DiscountService;
        readonly IBasketService BasketService;
        readonly IPriceFormatter PriceFormatter;

        public CheckoutScanner(IOperationFactory operationFactory,
            IConsole console,
            IProductService productService,
            IOrderService orderService,
            IDiscountService discountService,
            IBasketService basketService,
            IPriceFormatter priceFormatter,
            ILogger logger)
        {
            OperatorFactory = operationFactory;
            CheckoutConsole = console;
            ProductService = productService;
            OrderService = orderService;
            DiscountService = discountService;
            BasketService = basketService;
            PriceFormatter = priceFormatter;
            Logger = logger;
        }

        /// <summary>
        /// Initialises the scanner loading products and discounts.
        /// </summary>
        public void Initialise()
        {
            var operation = OperatorFactory.GetOperation(CheckoutConsole,
                ProductService,
                DiscountService,
                BasketService,
                PriceFormatter,
                OrderService,
                OperationType.INIT,
                Logger);

            operation.Execute();
        }

        /// <summary>
        /// Takes input from the scanner.
        /// </summary>
        /// <param name="input"></param>
        public void ReceiveInput(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var operation = OperatorFactory.GetOperation(CheckoutConsole,
                    ProductService,
                    DiscountService,
                    BasketService,
                    PriceFormatter,
                    OrderService,
                    input,
                    Logger);

                operation.Execute();
            }
        }
    }
}