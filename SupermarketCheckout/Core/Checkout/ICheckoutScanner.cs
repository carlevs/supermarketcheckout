﻿namespace SupermarketCheckout.Core.Checkout
{
    public interface ICheckoutScanner
    {
        void Initialise();
        void ReceiveInput(string input);
    }
}
