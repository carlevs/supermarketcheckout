﻿namespace SupermarketCheckout.Core.Checkout
{
    public interface IConsole
    {
        void WriteLine(string message);
        string ReadLine();
        void Quit();
    }
}
