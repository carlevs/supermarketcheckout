﻿using System;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    /// <summary>
    /// Completes the checkout for the current products in the basket,
    /// generating an order and displaying to user.
    /// </summary>
    public class CheckoutOperation : IOperation
    {
        readonly ILogger Logger;
        readonly IConsole CheckoutConsole;
        readonly IOrderService OrderService;
        readonly IBasketService BasketService;
        readonly IPriceFormatter PriceFormatter;

        public CheckoutOperation(IConsole console, IOrderService orderService,
            IBasketService basketService, IPriceFormatter priceFormatter,
            ILogger logger)
        {
            CheckoutConsole = console;
            OrderService = orderService;
            BasketService = basketService;
            PriceFormatter = priceFormatter;
            Logger = logger;
        }

        public void Execute()
        {
            Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                "Starting checkout process");

            // Get basket
            Basket basket = GetBasket();
            if (basket == null || basket.ProductCount == 0)
            {
                CheckoutConsole.WriteLine("You have no items to checkout");
                return;
            }

            // Create Order
            Order order = CreateOrder(basket);

            // Display order
            DisplayOrder(order);

            Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                "Checkout complete.  Exiting application");

            CheckoutConsole.Quit();
        }

        private Basket GetBasket()
        {
            return BasketService.GetBasket();

        }

        /// <summary>
        /// Creates an order from a basket of products.
        /// </summary>
        /// <param name="basket"></param>
        /// <returns>Populated Order</returns>
        private Order CreateOrder(Basket basket)
        {
            Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                "Creating order from basket\n" + basket.ToString());

            return OrderService.CreateOrder(basket.Products);
        }

        /// <summary>
        /// Displays a created order to the user.
        /// </summary>
        /// <param name="order"></param>
        private void DisplayOrder(Order order)
        {
            if (order == null)
                throw new Exception($"{order} is null");

            Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                "Displaying order");

            CheckoutConsole.WriteLine("--------------------");
            CheckoutConsole.WriteLine("Order Complete");
            CheckoutConsole.WriteLine("--------------------");

            foreach (var orderItem in order.OrderItems)
            {
                CheckoutConsole.WriteLine(orderItem.ToString());
            }

            CheckoutConsole.WriteLine("--------------------");
            CheckoutConsole.WriteLine($"Order Total:\t{PriceFormatter.Format(order.OrderTotal)}");
        }
    }
}
