﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Service;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    /// <summary>
    /// Displays basket content to the user.
    /// </summary>
    public class DisplayBasketOperation : IOperation
    {
        readonly IConsole CheckoutConsole;
        readonly IBasketService BasketService;
        readonly ILogger Logger;

        public DisplayBasketOperation(IConsole console, IBasketService basketService,
            ILogger logger)
        {
            CheckoutConsole = console;
            BasketService = basketService;
            Logger = logger;
        }

        public void Execute()
        {
            Logger.Log(LogLevel.INFO, typeof(DisplayBasketOperation),
                "Displaying basket...");

            var basket = BasketService.GetBasket();
            if (basket != null)
            {
                CheckoutConsole.WriteLine("Basket");
                CheckoutConsole.WriteLine("--------------------");

                if (basket.Products != null && basket.Products.Count > 0)
                {
                    foreach (var product in basket.Products)
                    {
                        CheckoutConsole.WriteLine(product.ToString());
                    }
                }
                else
                {
                    CheckoutConsole.WriteLine("Basket is currently empty");
                }
                CheckoutConsole.WriteLine("");
            }
        }
    }
}
