﻿namespace SupermarketCheckout.Core.Checkout.Operation
{
    public interface IOperation
    { 
        void Execute();
    }
}
