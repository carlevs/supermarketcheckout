﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Service;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    public interface IOperationFactory
    {
        IOperation GetOperation(IConsole console, IProductService productService,
            IDiscountService discountService, IBasketService basketService,
            IPriceFormatter priceFormatter, IOrderService orderService,
            string operatorType, ILogger logger);
    }
}
