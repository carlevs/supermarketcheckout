﻿using System.Collections.Generic;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    /// <summary>
    /// Initialises the checkout ready for input.
    /// </summary>
    public class InitialiseOperation : IOperation
    {
        readonly IConsole CheckoutConsole;
        readonly IProductService ProductService;
        readonly IDiscountService DiscountService;
        readonly ILogger Logger;

        public InitialiseOperation(IConsole console, IProductService productService,
            IDiscountService discountService, ILogger logger)
        {
            CheckoutConsole = console;
            ProductService = productService;
            DiscountService = discountService;
            Logger = logger;
        }

        public void Execute()
        {
            Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                "Initialising checkout...");

            IList<Product> productList = FindProducts();

            if (productList != null)
            {
                FindDiscounts();

                CheckoutConsole.WriteLine("");
                CheckoutConsole.WriteLine("Please enter a Sku, 'view' to display basket, " +
                    "'check' to checkout or 'quit' to exit");
            }
            else
            {
                CheckoutConsole.WriteLine("There are no products available");
            }
        }

        /// <summary>
        /// Looks for products in the database.
        /// </summary>
        /// <returns></returns>
        public IList<Product> FindProducts()
        {
            IList<Product> productList = ProductService.GetProducts();

            if (productList != null)
            {

                Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                    $"Found {productList.Count} products");

                CheckoutConsole.WriteLine("Welcome!");
                CheckoutConsole.WriteLine("Here are today's available products:");
                CheckoutConsole.WriteLine("------------------------");

                foreach (Product product in productList)
                {
                    // Display to user
                    CheckoutConsole.WriteLine(product.ToString());

                    Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                        product.ToString());
                }
            }

            return productList;
        }

        /// <summary>
        /// Looks for discounts in the database
        /// </summary>
        /// <returns></returns>
        public IList<Discount> FindDiscounts()
        {
            IList<Discount> discountList = DiscountService.GetDiscounts();

            if (discountList != null)
            {
                Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                    $"Found {discountList.Count} discounts");

                CheckoutConsole.WriteLine("");
                CheckoutConsole.WriteLine("This weeks offers:");
                CheckoutConsole.WriteLine("");

                foreach (var discount in discountList)
                {
                    // Display to user
                    CheckoutConsole.WriteLine(discount.ToString());

                    Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                        discount.ToString());
                }
            }
            else
            {
                Logger.Log(LogLevel.INFO, typeof(CheckoutOperation),
                       "No discounts found.");
            }

            return discountList;
        }
    }
}
