﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Service;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    public class OperationFactory : IOperationFactory
    {
        public IOperation GetOperation(IConsole console, IProductService productService,
            IDiscountService discountService, IBasketService basketService,
            IPriceFormatter priceFormatter, IOrderService orderService,
            string operatorType, ILogger logger)
        {
            switch (operatorType.ToLower())
            {
                case OperationType.INIT:
                    return new InitialiseOperation(console, productService, discountService, logger);
                case OperationType.CHECKOUT:
                    return new CheckoutOperation(console, orderService, basketService, priceFormatter, logger);
                case OperationType.VIEW:
                    return new DisplayBasketOperation(console, basketService, logger);
                default:
                    // Pass entered parameter to scan item
                    return new ScanItemOperation(console, productService, basketService, operatorType, logger);
            }
        }
    }
}
