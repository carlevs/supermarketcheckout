﻿namespace SupermarketCheckout.Core.Checkout.Operation
{
    public static class OperationType
    {
        public const string INIT = "init";
        public const string QUIT = "quit";
        public const string CHECKOUT = "check";
        public const string VIEW = "view";
    }
}
