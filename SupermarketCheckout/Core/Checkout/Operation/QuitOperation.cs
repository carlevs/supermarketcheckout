﻿using System;
using SupermarketCheckout.Core.Util;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    /// <summary>
    /// Exits the checkout application.
    /// </summary>
    public class QuitOperation : IOperation
    {
        readonly ILogger Logger;

        public QuitOperation(ILogger logger)
        {
            Logger = logger;
        }

        public void Execute()
        {
            Logger.Log(LogLevel.INFO, typeof(QuitOperation),
                "Displaying order");

            Environment.Exit(0);
        }
    }
}
