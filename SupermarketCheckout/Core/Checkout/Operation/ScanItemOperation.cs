﻿using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Service;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Core.Checkout.Operation
{
    /// <summary>
    /// Scans an item and if present, adds to basket.
    /// </summary>
    public class ScanItemOperation : IOperation
    {
        readonly IConsole CheckoutConsole;
        readonly IProductService ProductService;
        readonly IBasketService BasketService;
        readonly string Input;
        readonly ILogger Logger;

        public ScanItemOperation(IConsole console,
            IProductService productService,
            IBasketService basketService,
            string input,
            ILogger logger)
        {
            ProductService = productService;
            BasketService = basketService;
            CheckoutConsole = console;
            Input = input;
            Logger = logger;
        }

        public void Execute()
        {
            Logger.Log(LogLevel.INFO, typeof(ScanItemOperation),
                $"Scanning for item '{Input}'");

            try
            {
                var product = ProductService.GetProduct(Input);
                if (product != null)
                {
                    CheckoutConsole.WriteLine($"{product.Sku} added to basket");
                    CheckoutConsole.WriteLine("");

                    Logger.Log(LogLevel.INFO, typeof(ScanItemOperation),
                        "Product found.  Adding to basket...");

                    BasketService.AddItemToBasket(product);
                }
            }
            catch (ProductNotFoundException pnfe)
            {
                Logger.Log(LogLevel.INFO, typeof(ScanItemOperation),
                    pnfe.Message);

                CheckoutConsole.WriteLine("");
                CheckoutConsole.WriteLine($"Unknown command {Input}");
                CheckoutConsole.WriteLine("");
            }
        }
    }
}
