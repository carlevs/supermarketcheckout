﻿using System.Collections.Generic;

namespace SupermarketCheckout.Core.Counter
{
    public class GroupByCounter<T> : IListGroupCounter<T>
    {
        public IDictionary<T, int> Group(IList<T> list)
        {
            var totals = new Dictionary<T, int>();
            foreach (T item in list)
            {
                if (totals.ContainsKey(item))
                {
                    totals[item]++;
                }
                else
                {
                    totals.Add(item, 1);
                }
            }

            return totals;
        }
    }
}
