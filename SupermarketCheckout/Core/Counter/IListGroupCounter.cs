﻿using System.Collections.Generic;

namespace SupermarketCheckout.Core.Counter
{
    public interface IListGroupCounter<T>
    {
        IDictionary<T, int> Group(IList<T> list);
    }
}
