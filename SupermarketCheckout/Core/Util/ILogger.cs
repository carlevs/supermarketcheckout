﻿using System;

namespace SupermarketCheckout.Core.Util
{
    public interface ILogger
    {
        void Log(LogLevel logLevel, Type context, string message);
        void LogWithException(LogLevel logLevel, Type context, string message, Exception exception);
    }
}
