﻿namespace SupermarketCheckout.Core.Util
{
    public interface IPriceFormatter
    {
        string Format(decimal price);
    }
}
