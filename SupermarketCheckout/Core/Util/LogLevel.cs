﻿namespace SupermarketCheckout.Core.Util
{
    public enum LogLevel { WARN, INFO, DEBUG, ERROR, FATAL }
}
