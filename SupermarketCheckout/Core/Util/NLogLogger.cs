﻿using System;
using NLog;

namespace SupermarketCheckout.Core.Util
{
    public class NLogLogger : ILogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Standard log to file.
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="context"></param>
        /// <param name="message"></param>
        public void Log(LogLevel logLevel, Type context, string message)
        {
            LogEventInfo eventInfo = new LogEventInfo(
                GetNLogLevel(logLevel),
                context.FullName, message);

            Logger.Log(context, eventInfo);
        }

        /// <summary>
        /// Log to file with exception.
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void LogWithException(LogLevel logLevel, Type context, string message, Exception exception)
        {
            LogEventInfo eventInfo = new LogEventInfo(
                GetNLogLevel(logLevel),
                context.FullName, null, message, null, exception);
            Logger.Log(context, eventInfo);
        }

        /// <summary>
        /// Gets the NLog logging level based on the interface log level provided.
        /// </summary>
        /// <param name="logLevel"></param>
        /// <returns></returns>
        private NLog.LogLevel GetNLogLevel(LogLevel logLevel)
        {
            NLog.LogLevel nLogLevel;
            switch (logLevel)
            {
                case LogLevel.DEBUG:
                    nLogLevel = NLog.LogLevel.Debug;
                    break;
                case LogLevel.WARN:
                    nLogLevel = NLog.LogLevel.Warn;
                    break;
                case LogLevel.INFO:
                    nLogLevel = NLog.LogLevel.Info;
                    break;
                case LogLevel.ERROR:
                    nLogLevel = NLog.LogLevel.Error;
                    break;
                case LogLevel.FATAL:
                    nLogLevel = NLog.LogLevel.Fatal;
                    break;
                default:
                    throw new ArgumentException("Log level not found");
            }
            return nLogLevel;
        }
    }
}
