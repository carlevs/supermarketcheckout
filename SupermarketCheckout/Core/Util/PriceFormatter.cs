﻿namespace SupermarketCheckout.Core.Util
{
    public class PriceFormatter : IPriceFormatter
    {
        /// <summary>
        /// Formats the price based on the amount.
        /// </summary>
        /// <param name="price"></param>
        /// <returns>formatted price</returns>
        public string Format(decimal price)
        {
            string formattedString = "";

            if (price == 0m)
            {
                formattedString = "0";
            }
            else
            {
                formattedString = "£" + price.ToString();
            }

            return formattedString;
        }
    }
}
