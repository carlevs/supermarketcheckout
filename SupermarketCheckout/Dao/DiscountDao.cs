﻿using System;
using System.Collections.Generic;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Dao
{
    public class DiscountDao : IDiscountDao
    {
        //
        // Hardcoding but would come from local file/database etc...
        //
        List<Discount> DiscountList = new List<Discount>
        {
            { new Discount("A", 3, 1.30m, DateTime.Now.AddDays(7), new PriceFormatter()) },
            { new Discount("B", 2, 0.45m, DateTime.Now.AddDays(7), new PriceFormatter()) }
        };

        readonly ILogger Logger;

        public DiscountDao(ILogger logger)
        {
            Logger = logger;
        }

        public IList<Discount> GetDiscounts()
        {
            
            List<Discount> discounts;
            try
            {
                discounts = DiscountList;
            }
            catch (Exception e)
            {
                Logger.LogWithException(LogLevel.ERROR, typeof(DiscountDao), e.Message, e);
                throw new CheckoutException("Error getting discounts", e);
            }

            return discounts; 
        }
    }
}