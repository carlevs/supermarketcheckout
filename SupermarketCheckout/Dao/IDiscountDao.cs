﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Dao
{
    public interface IDiscountDao
    {
        IList<Discount> GetDiscounts();
    }
}
