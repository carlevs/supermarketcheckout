﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Dao
{
    public interface IProductDao
    {
        List<Product> GetProducts();
        Product GetProduct(string sku);
    }
}
