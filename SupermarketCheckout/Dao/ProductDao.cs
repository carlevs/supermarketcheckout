﻿using System;
using System.Collections.Generic;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using static SupermarketCheckout.Core.Util.ILogger;

namespace SupermarketCheckout.Dao
{
    public class ProductDao : IProductDao
    {
        //
        // Would store in database
        //
        private List<Product> ProductList = new List<Product>
        {
            new Product("A", 0.50m, new PriceFormatter()),
            new Product("B", 0.30m, new PriceFormatter()),
            new Product("C", 0.70m, new PriceFormatter()),
            new Product("D", 0.20m, new PriceFormatter())
        };

        readonly ILogger Logger;

        public ProductDao(ILogger logger)
        {
            Logger = logger;
        }

        public Product GetProduct(string sku)
        {
            //
            // This would be a database call
            //
            foreach(Product p in ProductList)
            {
                if (sku.ToLower().Equals(p.Sku.ToLower()))
                {
                    return p;
                }
            }

            throw new ProductNotFoundException($"Product not found with sku: {sku}");
        }

        public List<Product> GetProducts()
        {
            List<Product> products;
            try
            {
                products = ProductList;
            }
            catch (Exception e)
            {
                Logger.LogWithException(LogLevel.ERROR, typeof(ProductDao),
                    e.Message, e);
                throw new CheckoutException("Error getting products", e);
            }

            return ProductList;
        }
    }
}
