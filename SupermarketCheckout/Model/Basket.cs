﻿using System.Collections.Generic;
using System.Text;
using SupermarketCheckout.Service;

namespace SupermarketCheckout.Model
{
    public class Basket : ICheckoutService
    {
        public IList<Product> Products { get; set; }
        public int ProductCount { get => Products.Count; }

        public Basket()
        {
            Products = new List<Product>();
        }

        public Basket(IList<Product> products)
        {
            Products = products;
        }

        public void AddProduct(Product product)
        {
            Products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            Products.Remove(product);
        }

        public void Clear()
        {
            Products.Clear();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (Product product in Products)
            {
                builder.AppendLine(product.ToString());
            }

            return builder.ToString();
        }
    }
}
