﻿using System;
using SupermarketCheckout.Core.Util;

namespace SupermarketCheckout.Model
{
    public class Discount
    {
        public string Sku { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime ExpiryDate { get; set; }
        readonly IPriceFormatter PriceFormatter;

        public Discount() {}

        public Discount(string sku, int quantity, decimal price, DateTime expiryDate,
            IPriceFormatter formatter)
        {
            Sku = sku;
            Quantity = quantity;
            Price = price;
            ExpiryDate = expiryDate;
            PriceFormatter = formatter;
        }

        public override string ToString()
        {
            return $"{Sku}: {Quantity} for {PriceFormatter.Format(Price)}";
        }
    }
}
