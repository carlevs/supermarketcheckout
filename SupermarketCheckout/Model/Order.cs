﻿using System;
using System.Collections.Generic;

namespace SupermarketCheckout.Model
{
    public class Order
    {
        public DateTime OrderDate { get; set; }
        public IList<OrderItem> OrderItems { get; set; }

        public decimal OrderTotal
        {
            get
            {
                decimal total = 0;
                foreach (OrderItem item in OrderItems) {
                    total += item.Price; 
                }
                return total;
            }
        }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public Order(DateTime orderDate, IList<OrderItem> orderItems)
        {
            OrderDate = orderDate;
            OrderItems = orderItems;
        }
    }
}
