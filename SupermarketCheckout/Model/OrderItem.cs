﻿namespace SupermarketCheckout.Model
{
    public class OrderItem
    {
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public decimal Price { get; set; }

        public OrderItem() { }

        public OrderItem(int quantity, Product product, decimal price)
        {
            Quantity = quantity;
            Product = product;
            Price = price;
        }

        public override string ToString()
        {
            return $"{Product.Sku}\t{Quantity}\t{Product.PriceFormatter.Format(Price)}";
        }
    }
}
