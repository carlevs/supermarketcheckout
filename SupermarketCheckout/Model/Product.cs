﻿using SupermarketCheckout.Core.Util;

namespace SupermarketCheckout.Model
{
    public class Product
    {
        public string Sku { get; set; }
        public decimal Price { get; set; }
        public IPriceFormatter PriceFormatter { get; } 

        public Product() { }

        public Product(string sku, decimal price, IPriceFormatter formatter)
        {
            Sku = sku;
            Price = price;
            PriceFormatter = formatter;
        }

        public override string ToString()
        {
            return $"Sku: {Sku}\t Price: {PriceFormatter.Format(Price)}";
        }
    }
}
