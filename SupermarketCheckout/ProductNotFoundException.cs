﻿using System;

namespace SupermarketCheckout
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException()
        {
        }

        public ProductNotFoundException(string message) : base(message)
        {
        }

        public ProductNotFoundException(string message, Exception e) : base(message, e)
        {
        }
    }
}
