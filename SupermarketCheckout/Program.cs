﻿using Autofac;
using SupermarketCheckout.Builder;
using SupermarketCheckout.Core.Calculator;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Counter;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Dao;
using SupermarketCheckout.Service;

namespace SupermarketCheckout
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // Build up our IoC container, registering our concrete dependencies
            IContainer container = BuildIocContainer();
            IConsole console = container.Resolve<IConsole>();

            // Startup the application
            CheckoutScanner checkoutScanner = container.Resolve<CheckoutScanner>();

            try
            {
                checkoutScanner.Initialise();
            }
            catch (CheckoutException ce)
            {
                console.WriteLine("Error initialising scanner " + ce.Message);
                console.Quit();
            }

            while (true)
            {
                string input = console.ReadLine();

                try
                {
                    checkoutScanner.ReceiveInput(input);
                }
                catch (CheckoutException ce)
                {
                    console.WriteLine(ce.Message);
                    console.Quit();
                }
            }
        }

        /// <summary>
        /// Builds IoC container, registering dependencies for the application.
        /// </summary>
        /// <returns></returns>
        private static IContainer BuildIocContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            // Core
            builder.RegisterType<IOperation>();
            builder.RegisterType<CheckoutScanner>();
            builder.RegisterType<CheckoutConsole>().As<IConsole>();
            builder.RegisterType<NLogLogger>().As<ILogger>();

            // Services
            builder.RegisterType<ProductService>().As<IProductService>().SingleInstance();
            builder.RegisterType<OrderService>().As<IOrderService>().SingleInstance();
            builder.RegisterType<DiscountService>().As<IDiscountService>().SingleInstance();
            builder.RegisterType<BasketService>().As<IBasketService>().SingleInstance();

            // DAO
            builder.RegisterType<ProductDao>().As<IProductDao>();
            builder.RegisterType<DiscountDao>().As<IDiscountDao>();

            builder.RegisterType<PriceFormatter>().As<IPriceFormatter>();
            builder.RegisterGeneric(typeof(GroupByCounter<>)).As(typeof(IListGroupCounter<>));
            builder.RegisterType<OrderItemBuilder>().As<IOrderItemBuilder>();

            // Inject factories in for strategies
            builder.RegisterType<OperationFactory>().As<IOperationFactory>();
            builder.RegisterType<PriceCalculatorFactory>().As<IPriceCalculatorFactory>();

            return builder.Build();
        }
    }
}
