﻿using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public class BasketService : IBasketService
    {
        readonly Basket Basket = new Basket();

        public Basket GetBasket()
        {
            return Basket;
        }

        public void AddItemToBasket(Product product)
        {
            Basket.AddProduct(product);
        }
    }
}
