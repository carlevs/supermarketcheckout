﻿using System.Collections.Generic;
using SupermarketCheckout.Dao;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public class DiscountService : IDiscountService
    {
        readonly IDiscountDao DiscountDao;

        public DiscountService(IDiscountDao discountDao)
        {
            DiscountDao = discountDao;
        }

        public IList<Discount> GetDiscounts()
        {
            return DiscountDao.GetDiscounts();
        }
    }
}
