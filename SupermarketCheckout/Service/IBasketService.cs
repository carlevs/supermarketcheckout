﻿using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public interface IBasketService
    {
        Basket GetBasket();
        void AddItemToBasket(Product product);
    }
}
