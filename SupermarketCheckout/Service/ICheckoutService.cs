﻿using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public interface ICheckoutService
    {
        void AddProduct(Product product);
        void RemoveProduct(Product product);
        void Clear();
    }
}
