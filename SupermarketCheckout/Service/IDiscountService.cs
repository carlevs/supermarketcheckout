﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public interface IDiscountService
    {
        IList<Discount> GetDiscounts();
    }
}
