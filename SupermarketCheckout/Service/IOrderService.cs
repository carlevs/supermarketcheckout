﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public interface IOrderService
    {
        Order CreateOrder(IList<Product> products);
    }
}
