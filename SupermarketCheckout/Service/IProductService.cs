﻿using System.Collections.Generic;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public interface IProductService
    {
        IList<Product> GetProducts();
        Product GetProduct(string sku);
    }
}
