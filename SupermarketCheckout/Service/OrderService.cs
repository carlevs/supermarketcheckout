﻿using System;
using System.Collections.Generic;
using SupermarketCheckout.Builder;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public class OrderService : IOrderService
    {
        readonly IDiscountService DiscountService;
        readonly IOrderItemBuilder OrderItemBuilder;

        public OrderService(IDiscountService discountService,
            IOrderItemBuilder orderItemBuilder)
        {
            DiscountService = discountService;
            OrderItemBuilder = orderItemBuilder;
        }

        /// <summary>
        /// Creates an order from a list of products applying any applicable
        /// discounts.
        /// </summary>
        /// <param name="products"></param>
        /// <returns>Completed order</returns>
        public Order CreateOrder(IList<Product> products)
        {
            IList<Discount> discounts = DiscountService.GetDiscounts();
            IList<OrderItem> orderItems = OrderItemBuilder.Build(products, discounts);

            return new Order(DateTime.Now, orderItems);
        }
    }
}
