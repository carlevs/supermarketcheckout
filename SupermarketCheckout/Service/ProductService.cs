﻿using System.Collections.Generic;
using SupermarketCheckout.Dao;
using SupermarketCheckout.Model;

namespace SupermarketCheckout.Service
{
    public class ProductService : IProductService
    {
        readonly IProductDao ProductDao;

        public ProductService(IProductDao productDao)
        {
            ProductDao = productDao;
        }

        public IList<Product> GetProducts()
        {
            return ProductDao.GetProducts();
        }

        public Product GetProduct(string sku)
        {
            return ProductDao.GetProduct(sku);
        }
    }
}
