﻿using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System;
using SupermarketCheckout.Service;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;

namespace Tests
{
    [TestFixture]
    public class TestCheckoutOperation
    {
        Mock<IOrderService> MockOrderService;
        Mock<IBasketService> MockBasketService;
        Mock<ILogger> MockLogger;
        Mock<IConsole> MockConsole;
        Product MockProduct;
        IList<Product> MockProductList;
        const string SKU_A = "A";

        [SetUp]
        public void SetUp()
        {
            MockOrderService = new Mock<IOrderService>();
            MockBasketService = new Mock<IBasketService>();
            MockLogger = new Mock<ILogger>();
            MockConsole = new Mock<IConsole>();
            MockProduct = new Product(SKU_A, 1.03m, new PriceFormatter());
            MockProductList = new List<Product> { MockProduct };
        }

        [Test]
        public void TestCheckoutOperationCompletes()
        {

            MockBasketService.Setup(b => b.GetBasket()).Returns(new Basket(MockProductList));
            MockOrderService.Setup(o => o.CreateOrder(MockProductList)).Returns(new Order());

            var operation = new CheckoutOperation(
                MockConsole.Object,
                MockOrderService.Object,
                MockBasketService.Object,
                new PriceFormatter(),
                MockLogger.Object);

            operation.Execute();

            MockBasketService.Verify(b => b.GetBasket(), Times.Once());
            MockOrderService.Verify(o => o.CreateOrder(MockProductList), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestCheckoutOperationReturnsWhenNullBasket()
        {
            MockBasketService.Setup(b => b.GetBasket()).Returns((Basket) null);
            MockOrderService.Setup(o => o.CreateOrder(MockProductList)).Returns(new Order());

            var operation = new CheckoutOperation(
                MockConsole.Object,
                MockOrderService.Object,
                MockBasketService.Object,
                new PriceFormatter(),
                MockLogger.Object);

            operation.Execute();

            MockBasketService.Verify(b => b.GetBasket(), Times.Once());
            MockOrderService.Verify(o => o.CreateOrder(MockProductList), Times.Never());
            Assert.Pass();
        }

        [Test]
        public void TestExceptionThrownWhenOrderIsNull()
        {
            MockBasketService.Setup(b => b.GetBasket()).Returns(new Basket(MockProductList));
            MockOrderService.Setup(o => o.CreateOrder(MockProductList)).Returns((Order) null);

            var operation = new CheckoutOperation(
                MockConsole.Object,
                MockOrderService.Object,
                MockBasketService.Object,
                new PriceFormatter(),
                MockLogger.Object);

            Assert.Throws<Exception>(() => operation.Execute());
        }

    }
}
