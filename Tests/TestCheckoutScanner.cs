﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SupermarketCheckout.Builder;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;

namespace Tests
{
    [TestFixture]
    public class TestCheckoutScanner
    {
        [Test]
        public void TestCanScanAndCompleteOrder()
        {
            var mockProductA = new Product("A", 0.50m, new PriceFormatter());
            var mockProductB = new Product("B", 0.30m, new PriceFormatter());
            var mockProductC = new Product("C", 0.70m, new PriceFormatter());
            var mockProductD = new Product("D", 0.20m, new PriceFormatter());
            var mockDiscountA = new Discount("A", 3, 1.30m, DateTime.Now.AddDays(7), new PriceFormatter());
            var mockDiscountB = new Discount("B", 2, 0.45m, DateTime.Now.AddDays(7), new PriceFormatter());
            var mockOrderItemBuilder = new Mock<IOrderItemBuilder>();

            var mockProductList = new List<Product> {
                mockProductA, mockProductB, mockProductC, mockProductD };

            var mockDiscountList = new List<Discount> { mockDiscountA, mockDiscountB };
      
            var mockProductService = new Mock<IProductService>();
            var mockOrderService = new Mock<IOrderService>();
            var mockBasketService = new Mock<IBasketService>();
            var mockDiscountService = new Mock<IDiscountService>();
            var console = new Mock<IConsole>();
            var mockLogger = new Mock<ILogger>();

            mockProductService.Setup(p => p.GetProducts())
                .Returns(mockProductList);
            mockDiscountService.Setup(d => d.GetDiscounts())
                .Returns(mockDiscountList);

            mockProductService.Setup(p => p.GetProduct(mockProductB.Sku))
                .Returns(mockProductB);
            mockProductService.Setup(p => p.GetProduct(mockProductA.Sku))
                .Returns(mockProductA);

            var selectedProductList = new List<Product> { mockProductB, mockProductA, mockProductB };
            mockBasketService.Setup(b => b.GetBasket())
                .Returns(new Basket(selectedProductList));

            mockOrderService.Setup(o => o.CreateOrder(selectedProductList))
                .Returns(new Order(It.IsAny<DateTime>(),
                new List<OrderItem> {
                    new OrderItem(2, mockProductB, 0.45m),
                    new OrderItem(1, mockProductA, 0.5m)
                }));

            var checkoutScanner = new CheckoutScanner(
                new OperationFactory(),
                console.Object,
                mockProductService.Object,
                mockOrderService.Object,
                mockDiscountService.Object,
                mockBasketService.Object,
                new PriceFormatter(),
                mockLogger.Object);

            // Initialise
            checkoutScanner.Initialise();

            mockProductService.Verify(p => p.GetProducts(), Times.Once());
            mockDiscountService.Verify(d => d.GetDiscounts(), Times.Once());

            // Scan products
            checkoutScanner.ReceiveInput("B");
            checkoutScanner.ReceiveInput("A");
            checkoutScanner.ReceiveInput("B");
            checkoutScanner.ReceiveInput("check");
            
            mockProductService.Verify(p => p.GetProduct(mockProductB.Sku), Times.Exactly(2));
            mockProductService.Verify(p => p.GetProduct(mockProductA.Sku), Times.Once());
            mockBasketService.Verify(b => b.AddItemToBasket(mockProductA), Times.Once());
            mockBasketService.Verify(b => b.AddItemToBasket(mockProductB), Times.Exactly(2));
            mockOrderService.Verify(o => o.CreateOrder(It.IsAny<List<Product>>()), Times.Once());

            Assert.Pass();
        }
    }
}
