﻿using System;
using Moq;
using NUnit.Framework;
using SupermarketCheckout.Core.Calculator;
using SupermarketCheckout.Model;
using SupermarketCheckout.Core.Util;

namespace Tests
{
    [TestFixture]
    public class TestDiscountPriceCalculator
    {
        Mock<ILogger> MockLogger;

        [SetUp]
        public void SetUp()
        {
            MockLogger = new Mock<ILogger>();
        }

        [Test]
        public void ShouldReturnCorrectPriceForExactlyOneValidDiscount()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(3, 0.5m);

            Assert.AreEqual(1.30m, result);
        }

        [Test]
        public void ShouldReturnCorrectPriceForExactlyTwoValidDiscounts()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(6, 0.5m);

            Assert.AreEqual(2.60m, result);
        }

        [Test]
        public void ShouldReturnCorrectPriceForOneValidDiscountAndOneFullPriceItem()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(4, 0.5m);

            Assert.AreEqual(1.80m, result);
        }

        [Test]
        public void ShouldReturnCorrectPriceForOneValidDiscountAndTwoFullPriceItems()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(5, 0.5m);

            Assert.AreEqual(2.30m, result);
        }

        [Test]
        public void ShouldReturnCorrectPriceForTwoValidDiscountsAndOneFullPriceItem()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(7, 0.5m);

            Assert.AreEqual(3.10m, result);
        }

        [Test]
        public void ShouldReturnCorrectPriceForTwoValidDiscountsAndTwoFullPriceItems()
        {
            var calculator = new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 1.30m },
                MockLogger.Object);
            decimal result = calculator.Calculate(8, 0.5m);

            Assert.AreEqual(3.60m, result);
        }

        [Test]
        public void ShouldThrowArgumentExceptionWithZeroDiscountQuantity()
        {
            Assert.Throws<ArgumentException>(
                          () => new DiscountPriceCalculator(new Discount { Quantity = 0, Price = 1.30m },
                          MockLogger.Object));
        }

        [Test]
        public void ShouldThrowArgumentExceptionWithZeroDiscountPrice()
        {
            Assert.Throws<ArgumentException>(
                          () => new DiscountPriceCalculator(new Discount { Quantity = 3, Price = 0m },
                          MockLogger.Object));
        }

    }
}
