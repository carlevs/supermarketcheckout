﻿using Moq;
using NUnit.Framework;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class TestDisplayBasketOperation
    {
        Mock<IBasketService> MockBasketService = new Mock<IBasketService>();
        Mock<IConsole> MockConsole = new Mock<IConsole>();
        Mock<ILogger> MockLogger = new Mock<ILogger>();

        [SetUp]
        public void SetUp()
        {
            MockBasketService = new Mock<IBasketService>();
            MockConsole = new Mock<IConsole>();
            MockLogger = new Mock<ILogger>();
        }

        [Test]
        public void TestNullBasketThrowsException()
        {
            MockBasketService.Setup(b => b.GetBasket()).Returns((Basket)null);

            var displayBasketOperation = new DisplayBasketOperation(
                MockConsole.Object,
                MockBasketService.Object,
                MockLogger.Object);

            displayBasketOperation.Execute();

            MockBasketService.Verify(b => b.GetBasket(), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestEmptyBasketIsDisplayed()
        {
            MockBasketService.Setup(b => b.GetBasket()).Returns(new Basket());

            var displayBasketOperation = new DisplayBasketOperation(
                MockConsole.Object,
                MockBasketService.Object,
                MockLogger.Object);

            displayBasketOperation.Execute();

            MockBasketService.Verify(b => b.GetBasket(), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestBasketIsDisplayedWithProducts()
        {
            MockBasketService.Setup(b => b.GetBasket()).Returns(new Basket(
                new List<Product>() {
                    new Product("A", 1.0m, new PriceFormatter()),
                    new Product("B", 1.4m, new PriceFormatter()),
                    new Product("C", 1.6m, new PriceFormatter())
                }));

            var displayBasketOperation = new DisplayBasketOperation(
                MockConsole.Object,
                MockBasketService.Object,
                MockLogger.Object);

            displayBasketOperation.Execute();

            MockBasketService.Verify(b => b.GetBasket(), Times.Once());
            Assert.Pass();
        }
    }
}
