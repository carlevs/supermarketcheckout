﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using SupermarketCheckout.Core.Counter;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;

namespace Tests
{
    [TestFixture]
    public class TestGroupByCounter
    {
        [Test]
        public void ShouldReturnCountOfZeroProducts()
        {
            var productList = new List<Product>();

            var counter = new GroupByCounter<Product>();
            IDictionary<Product, int> groupedProducts = counter.Group(productList);

            Assert.AreEqual(0, groupedProducts.Count);
        }

        [Test]
        public void ShouldReturnCountOf3Products()
        {
            var product = new Product("A", 0.5m, new PriceFormatter());
            var productList = new List<Product> { product, product, product };

            var counter = new GroupByCounter<Product>();
            IDictionary<Product, int> groupedProducts = counter.Group(productList);

            Assert.IsTrue(groupedProducts.ContainsKey(product));
            Assert.AreEqual(1, groupedProducts.Count);
            Assert.AreEqual(3, groupedProducts[product]);
        }

        [Test]
        public void ShouldReturnCountOf5Discounts()
        {
            var discount = new Discount("A", 3, 1.3m, DateTime.Now, new PriceFormatter());
            var discountList = new List<Discount> {
                discount, discount, discount, discount, discount };

            var counter = new GroupByCounter<Discount>();
            IDictionary<Discount, int> groupedDiscounts = counter.Group(discountList);

            Assert.IsTrue(groupedDiscounts.ContainsKey(discount));
            Assert.AreEqual(1, groupedDiscounts.Count);
            Assert.AreEqual(5, groupedDiscounts[discount]);
        }
    }
}
