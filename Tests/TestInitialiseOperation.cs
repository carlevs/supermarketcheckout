﻿using Moq;
using NUnit.Framework;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class TestInitialiseOperation
    {
        Mock<IProductService> MockProductService;
        Mock<IDiscountService> MockDiscountService;
        Mock<IConsole> MockConsole;
        Mock<ILogger> MockLogger;
        const string SKU_A = "A";


        [SetUp]
        public void SetUp()
        {
            MockProductService = new Mock<IProductService>();
            MockDiscountService = new Mock<IDiscountService>();
            MockConsole = new Mock<IConsole>();
            MockLogger = new Mock<ILogger>();
        }

        [Test]
        public void TestScannerIsInitialisedWithNoProductsOrDiscounts()
        {
            MockProductService.Setup(p => p.GetProducts()).Returns(new List<Product>());
            MockDiscountService.Setup(m => m.GetDiscounts()).Returns(new List<Discount>());

            var operation = new InitialiseOperation(
                MockConsole.Object,
                MockProductService.Object,
                MockDiscountService.Object,
                MockLogger.Object);

            operation.Execute();

            MockProductService.Verify(p => p.GetProducts(), Times.Once());
            MockDiscountService.Verify(d => d.GetDiscounts(), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestScannerIsInitialisedWithProductsNoDiscounts()
        {
            MockProductService.Setup(p => p.GetProducts()).Returns(new List<Product>());
            MockDiscountService.Setup(m => m.GetDiscounts()).Returns((List<Discount>) null);

            var operation = new InitialiseOperation(
                MockConsole.Object,
                MockProductService.Object,
                MockDiscountService.Object,
                MockLogger.Object);

            operation.Execute();

            MockProductService.Verify(p => p.GetProducts(), Times.Once());
            MockDiscountService.Verify(d => d.GetDiscounts(), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestScannerIsNotInitialisedWithNoProducts()
        {
            MockProductService.Setup(p => p.GetProducts()).Returns((List<Product>) null);

            var operation = new InitialiseOperation(
                MockConsole.Object,
                MockProductService.Object,
                MockDiscountService.Object,
                MockLogger.Object);

            operation.Execute();

            MockProductService.Verify(p => p.GetProducts(), Times.Once());
            MockDiscountService.Verify(d => d.GetDiscounts(), Times.Never());
            Assert.Pass();
        }
    }
}
