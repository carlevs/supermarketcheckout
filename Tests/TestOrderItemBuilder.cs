﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SupermarketCheckout.Builder;
using SupermarketCheckout.Core.Calculator;
using SupermarketCheckout.Core.Counter;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;

namespace Tests
{
    [TestFixture]
    public class TestOrderItemBuilder
    {
        Mock<ILogger> MockLogger;

        [SetUp]
        public void SetUp()
        {
            MockLogger = new Mock<ILogger>();
        }

        [Test]
        public void ShouldReturnExceptionWithNullLists()
        {
            var builder = new OrderItemBuilder(new GroupByCounter<Product>(),
                new PriceCalculatorFactory(), MockLogger.Object);
            Assert.Throws<ArgumentNullException>(() => builder.Build(null, null));
        }

        [Test]
        public void ShouldReturnExceptionWithNullProducts()
        {
            var builder = new OrderItemBuilder(new GroupByCounter<Product>(), new PriceCalculatorFactory(),
                MockLogger.Object);
            Assert.Throws<ArgumentNullException>(() => builder.Build(null, new List<Discount>()));
        }

        [Test]
        public void ShouldReturnEmptyList()
        {
            var productList = new List<Product>();
            var discountList = new List<Discount>();

            var builder = new OrderItemBuilder(new GroupByCounter<Product>(), new PriceCalculatorFactory(),
                MockLogger.Object);
            IList<OrderItem> orderItemList = builder.Build(productList, discountList);

            Assert.IsNotNull(orderItemList);
            Assert.AreEqual(0, orderItemList.Count);
        }

        [Test]
        public void ShouldBuildOneOrderItemWithNoDiscount()
        {
            var productList = new List<Product> { new Product("A", 0.5m, new PriceFormatter()) };
            var discountList = new List<Discount>();

            var builder = new OrderItemBuilder(new GroupByCounter<Product>(), new PriceCalculatorFactory(),
                MockLogger.Object);
            IList<OrderItem> orderItemList = builder.Build(productList, discountList);

            Assert.IsNotNull(orderItemList);
            Assert.AreEqual(1, orderItemList.Count);
            Assert.AreEqual(0.50, orderItemList[0].Price);
        }

        [Test]
        public void ShouldBuildOneOrderItemWithDiscount()
        {
            var productA = new Product("A", 0.5m, new PriceFormatter());
            var discount = new Discount("A", 3, 1.30m, DateTime.Now.AddDays(7), new PriceFormatter());

            var productList = new List<Product> {
                productA,
                productA,
                productA
            };

            var discountList = new List<Discount> { discount };

            var builder = new OrderItemBuilder(new GroupByCounter<Product>(), new PriceCalculatorFactory(),
                MockLogger.Object);
            IList<OrderItem> orderItemList = builder.Build(productList, discountList);

            Assert.IsNotNull(orderItemList);
            Assert.AreEqual(1, orderItemList.Count);
            Assert.AreEqual(1.30, orderItemList[0].Price);
        }

        [Test]
        public void ShouldBuildTwoOrderItemsOneWithDiscount()
        {
            var productA = new Product("A", 0.5m, new PriceFormatter());
            var productB = new Product("B", 0.3m, new PriceFormatter());
            var discountA = new Discount("A", 3, 1.30m, DateTime.Now.AddDays(7), new PriceFormatter());
            var discountB = new Discount("B", 2, 0.45m, DateTime.Now.AddDays(7), new PriceFormatter());

            var productList = new List<Product> {
                productB,
                productA,
                productB
            };

            var discountList = new List<Discount> { discountA, discountB };

            var builder = new OrderItemBuilder(new GroupByCounter<Product>(), new PriceCalculatorFactory(),
                MockLogger.Object);
            IList<OrderItem> orderItemList = builder.Build(productList, discountList);

            Assert.IsNotNull(orderItemList);
            Assert.AreEqual(2, orderItemList.Count);
            Assert.AreEqual(0.45, orderItemList[0].Price);
            Assert.AreEqual(0.5, orderItemList[1].Price);
        }
    }
}
