﻿using NUnit.Framework;
using SupermarketCheckout.Core.Util;

namespace Tests
{
    [TestFixture]
    public class TestPriceFormatter
    {
        [Test]
        public void ShouldReturnZero()
        {
            PriceFormatter priceFormatter = new PriceFormatter();
            string result = priceFormatter.Format(0);

            Assert.AreEqual("0", result);
        }

        [Test]
        public void ShouldReturnPenceValue()
        {
            PriceFormatter priceFormatter = new PriceFormatter();
            string result = priceFormatter.Format(0.99m);

            Assert.AreEqual("£0.99", result);
        }

        [Test]
        public void ShouldReturnPoundsAndPence()
        {
            PriceFormatter priceFormatter = new PriceFormatter();
            string result = priceFormatter.Format(1.35m);

            Assert.AreEqual("£1.35", result);
        }

        [Test]
        public void ShouldReturnPoundValue()
        {
            PriceFormatter priceFormatter = new PriceFormatter();
            string result = priceFormatter.Format(1m);

            Assert.AreEqual("£1", result);
        }

        [Test]
        public void ShouldReturnHundredPoundValue()
        {
            PriceFormatter priceFormatter = new PriceFormatter();
            string result = priceFormatter.Format(100m);

            Assert.AreEqual("£100", result);
        }

    }
}
