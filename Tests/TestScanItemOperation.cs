﻿using Moq;
using NUnit.Framework;
using SupermarketCheckout.Core.Checkout;
using SupermarketCheckout.Core.Checkout.Operation;
using SupermarketCheckout.Core.Util;
using SupermarketCheckout.Model;
using SupermarketCheckout.Service;

namespace Tests
{
    [TestFixture]
    public class TestScanItemOperation
    {
        Mock<IProductService> MockProductService;
        Mock<IBasketService> MockBasketService;
        Mock<IConsole> MockConsole;
        Mock<ILogger> MockLogger;
        const string SKU_A = "A";
        Product MockProduct;

        [SetUp]
        public void SetUp()
        {
            MockProductService = new Mock<IProductService>();
            MockBasketService = new Mock<IBasketService>();
            MockConsole = new Mock<IConsole>();
            MockLogger = new Mock<ILogger>();
            MockProduct = new Product(SKU_A, 1.03m, new PriceFormatter());
        }

        [Test]
        public void TestScanItemCompletesWithNoExceptions()
        {
            MockProductService.Setup(p => p.GetProduct(SKU_A)).Returns(MockProduct);
            MockBasketService.Setup(b => b.AddItemToBasket(MockProduct));

            var scanItemOperation = new ScanItemOperation(
                MockConsole.Object,
                MockProductService.Object,
                MockBasketService.Object,
                SKU_A,
                MockLogger.Object);

            scanItemOperation.Execute();

            MockProductService.Verify(p => p.GetProduct(SKU_A), Times.Once());
            MockBasketService.Verify(b => b.AddItemToBasket(MockProduct), Times.Once());
            Assert.Pass();
        }

        [Test]
        public void TestItemIsNotAddedToBasketWhenProductNotFound()
        {
            MockProductService.Setup(p => p.GetProduct(SKU_A)).Returns((Product)null);
            MockBasketService.Setup(b => b.AddItemToBasket(MockProduct));

            var scanItemOperation = new ScanItemOperation(
                MockConsole.Object,
                MockProductService.Object,
                MockBasketService.Object,
                SKU_A,
                MockLogger.Object);

            scanItemOperation.Execute();

            MockProductService.Verify(p => p.GetProduct(SKU_A), Times.Once());
            MockBasketService.Verify(b => b.AddItemToBasket(MockProduct), Times.Never());
            Assert.Pass();
        }
    }
}
