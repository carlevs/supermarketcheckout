﻿using System;
using NUnit.Framework;
using SupermarketCheckout.Core.Calculator;

namespace Tests
{
    [TestFixture]
    public class TestStandardPriceCalculator
    {

        [Test]
        public void ShouldReturnExceptionForNegativeValue()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            Assert.Throws<ArgumentException>(() => calculator.Calculate(0, -0.45m));
        }

        [Test]
        public void ShouldReturnExceptionWithNoCount()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            Assert.Throws<ArgumentException>(() => calculator.Calculate(0, 1.99m));
        }

        [Test]
        public void ShouldReturnExceptionWithNoPrice()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            Assert.Throws<ArgumentException>(() => calculator.Calculate(3, 0m));
        }

        [Test]
        public void ShouldReturnZeroWithNoCountNoPrice()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            Assert.Throws<ArgumentException>(() => calculator.Calculate(0, 0m));
        }

        [Test]
        public void ShouldReturnValidCalculatedPrice()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            decimal result = calculator.Calculate(3, 0.5m);

            Assert.AreEqual(1.5m, result);
        }

        [Test]
        public void ShouldReturnValidCalculatedPriceWholeNumber()
        {
            StandardPriceCalculator calculator = new StandardPriceCalculator();
            decimal result = calculator.Calculate(3, 20m);

            Assert.AreEqual(60m, result);
        }
    }
}
